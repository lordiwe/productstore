<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
	'title',
	'image',
	'price'];

    public function getTitle()
    {
	return $this->title;
    }

    public function getImage()
    {
	return $this->image;
    }

    public function getPrice()
    {
	return $this->price;
    }
}
