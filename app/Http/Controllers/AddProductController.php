<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class AddProductController extends Controller
{
    public function create()
    {
	return view('create');
    }

    public function store(Request $request)
    {
	$this->validate({
	'title' => 'required|min:5|max:100',
	'image' => 'required',
	'price' => 'required',
	]);
	Product::create([
	'title' => $request->input('title'),
	'image' => $request->input('image'),
	'price' => $request->input('price')
    ]);
	$products = array($product => ['title' => $this->title,
					'image' => $this->image,
					'price' => $this->price]

	return redirect()->('home')->with('info', 'product created');
    }

}
