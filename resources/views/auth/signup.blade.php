@extends('templates.default')

@section('content')
    <div class="row">
    <div class="col-lg-4 mx-auto"
    <h3>Registration</h3>
    <form method="POST" action="{{ route('auth.signup') }}" novalidate>
    @csrf
  <div class="form-group">
    <label for="emai1">Email address</label>
    <input type="email" name="email" class="form-control{{ $errors->has('email') ? 'is-invalid : ''" 
    id="email" placeholder="name@email.com"
    value="{{ Request::old('email') ?: '' }}">

    @if	($errors->has('email'))
	<span class="help-block text-danger">
	    {{ $errors->first('email') }}
	</span>
    @endif
  </div>
    <div class="form-group">
    <label for="username">Login</label>
    <input type="text" name="username" class="form-control{{ $errors->has('username') ? 'is-invalid : ''"
     id="password" placeholder="Enter nickname"
    value="{{ Request::old('username') ?: '' }}">

	@if>($errors->has('usarname'))
	<span class="help-block text-danger">
        {{ $errors->first('username') }}
	</span>
    @endif

     </div>
  <div class="form-group">
    <label for="password">Password</label>
    <input type="password" name="password" class="form-control{{ $errors->has('password') ? 'is-invalid : ''" 
    id="password" placeholder="Enter password">

    @if>($errors->has('password'))
        {{ $errors->first('password') }}
	</span>
    @endif

  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
</div>
@endsection