<nav class="navbar navbar-expand-lg navbar-dark bg-dark mb-4">
    <div class="container">
  <a class="navbar-brand" href="#">Product Store</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">

    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    @if (Auth::check())
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="home">Home page<span class="sr-only">(current)</span></a>
      </li>
	<li class="nav-item active">
        <a class="nav-link" href="{{ route('form.create') }}">Add Product<span class="sr-only">(current)</span></a>
      </li>

    </ul>
    @endif
    <ul class="navbar-nav ml-auto">
    @if (Auth::check())
	<li class="nav-item"><a href="#" class="nav-link">{{ Auth::user()->getNameOrUsername() }}</a></li>
	<li class="nav-item"><a href="#" class="nav-link">Logot</a></li>
    @else
	<li class="nav-item"><a href="{{ route('auth.signup') }}" class="nav-link">Sign Up</a></li>
	<li class="nav-item"><a href="{{ route('auth.signin') }}" class="nav-link">Sign In</a></li>
    @endif
    </ul>
    </div>
  </div>
</nav>

