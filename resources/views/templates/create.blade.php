@extends('default')

@section('content')
    <style>
    .uper {
	margin-top: 40px;
    }
    </style>
    <div class="card uper">
	<div class="card-header">
	    Add Product
	</div>
	<div class="card-body">
	@if ($errors->any())
	<div class="alert alert-danger">
	    <ul>
		@foreach ($errors->all() as $error)
		    <li>{{ $error }}</li>
		@endforeach
	    </ul>
	</div>
	<br />
	@endif
    <form method="post" action="{{ route('form.store') }}">
	@csrf
	<div class="form-group">
	    <label for="title">Product Title:</label>
	    <input type="text" class="form-control" name="title"
	</div>
	<div class="form-group">
	    <input type="file" name="image" class="form-control-image">
	</div>
	<div class="form-group">
	    <label for="title">Product Price:</label>
	    <input type="text" class="form-control" name="price"
	</div>
	<button type="submit" class="btn btn-dark">Add Product</button>
    </form>
    </div>
</div>


@endsection